
/// PARAMETERS ///
int WIDTH = 380;
int HEIGHT = 2000;
int CELL_WIDTH = 10;
int CELL_HEIGHT = 10;
int CELL_IMG_NUM = 12;
int CA_SIZE = 38;
int CA_STATENUM = 4;
int GEN_NUM = 200;
//////////////////



class CA {
  int Size, StateNum;
  int[][][] Rules;
  int[] Cells;
  CA(int size, int n) {
    Size = size;
    StateNum = n;
    // init ruleset
    Rules = new int[n][n][n];
    for (int i=0; i<n; i++)
      for (int j=0; j<n; j++)
        for (int k=0; k<n; k++)
          Rules[i][j][k] = floor(random(n));
    // init cells
    Cells = new int[size];
    for (int i=0; i<size; i++)
      Cells[i] = floor(random(n));
  }
  void next() {
    int[] nextCells = new int[Size];
    nextCells[0] = Rules[Cells[Size-1]][Cells[0]][Cells[1]];
    for (int i=1; i<Size-1; i++)
      nextCells[i] = Rules[Cells[i-1]][Cells[i]][Cells[i+1]];
    nextCells[Size-1] = Rules[Cells[Size-2]][Cells[Size-1]][Cells[0]];
    Cells = nextCells;
  }
  void print() {
    String out = "";
    for (int i=0; i<Size; i++)
      out = out + nf(Cells[i]);
    println(out);
  }
}

void setup() {
  // init
  surface.setVisible(false);
  PGraphics outImg = createGraphics(WIDTH, HEIGHT);
  CA ca = new CA(CA_SIZE, CA_STATENUM);
  PImage[] CellImgs = new PImage[CA_STATENUM];
  int[] cellImgsIndex = new int[CA_STATENUM];
  for (int i=0; i<CA_STATENUM; i++) {
    int r = floor(random(CELL_IMG_NUM));
    boolean p = true;
    while (p) {
      r = floor(random(CELL_IMG_NUM));
      p = false;
      for (int j=0; j<i; j++)
        p = p || cellImgsIndex[j] == r;
    }
    println(nf(r));
    cellImgsIndex[i] = r;
    CellImgs[i] = loadImage("stateImgs/"+ nf(r) + ".png");
  }
  // render
  outImg.beginDraw();
  for (int i=0; i<GEN_NUM; i++) {
    for (int j=0; j<CA_SIZE; j++)
      outImg.image(CellImgs[ca.Cells[j]], j*CELL_WIDTH, i*CELL_HEIGHT);
    ca.next();
  }
  outImg.endDraw();
  outImg.save("ca.png");
  println("Finished.");
  exit();
}
